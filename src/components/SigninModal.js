import React, { Component } from "react";
import "../asset/asset.css";

export default class SigninModal extends Component {
  render() {
    if (!this.props.show) {
      return null;
    }
    return (
      <div className="modal-box">
        <div className="logo-block">
          <div className="logo-block-1">
            <div className="tv">
              <h4>Belinda's TV</h4>
            </div>
          </div>
        </div>
        <div className="fullname">
          <h5>Full Name</h5>
          <input className="input-1" type="name" name="name" placeholder="" />
        </div>
        <div className="email">
          <h5>Email</h5>
          <input className="input-2" type="email" name="email" placeholder="" />
        </div>
        <div className="password">
          <h5>Password</h5>
          <input
            className="input-3"
            type="password"
            name="password"
            placeholder=""
          />
        </div>
        <button className="button">
          <h4>Sign Up</h4>
        </button>
        <div className="account">
          <h5>
            Already have an account?
            <button>Login</button>
          </h5>
        </div>
      </div>
    );
  }
}
