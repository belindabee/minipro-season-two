import React, { Component } from "react";

export default class Sypnosis extends Component {
  render() {
    return (
      <div className="sypnosis">
        <p>
          Saint Seiya (聖闘士星矢 Seinto Seiya) atau judul lengkapnya Saint
          Seiya: Kesatria-kesatria Zodiak atau hanya Kesatria-kesatria Zodiak
          adalah anime dan manga karya Masami Kurumada.
        </p>
        <div className="trailer">
          <button>Watch Trailer</button>
        </div>
        <div className="whislist">
          <button>Whistlist</button>
        </div>
      </div>
    );
  }
}
