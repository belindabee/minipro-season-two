import React, { Component } from "react";
import "../asset/asset.css";
import SigninModal from "./SigninModal";

export default class SignInMenu extends Component {
  state = {
    show: false
  };

  showModal = e => {
    let show = this.state.show === true ? false : true;
    this.setState({
      show
    });
  };
  render() {
    return (
      <div className="signin">
        <button
          onClick={e => {
            this.showModal();
          }}
        >
          Sign In
        </button>
        <SigninModal show={this.state.show} />
      </div>
    );
  }
}
