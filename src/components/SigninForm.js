import React from "react";
import SignInMenu from "../components/SignInMenu";
import "../asset/asset.css";

export default class SigninForm extends React.Component {
  render() {
    if (!this.props.show) {
      return null;
    }
    return (
      <div>
        <SignInMenu show={this.state.show} />
        <div className="signin-menu"></div>
        <div></div>
      </div>
    );
  }
}
