import Axios from 'axios';

export const login = (data) => {
    return Axios({
        method: 'PUT',
        url: 'http://yang-keren-gitu-napa.herokuapp.com/users',
        data: data
    });
}

export const moviesPost = (data) => {
    return Axios({
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': `${localStorage.getIem('token')}`,
        },
        url: 'http://yang-keren-gitu-napa.herokuapp.com/movies',
        data: data
    });
}