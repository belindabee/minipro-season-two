import React, { Component } from "react";

export default class MiddleFooter extends Component {
  render() {
    return (
      <div>
        <div className="tentang">
          <h5>Tentang Kami</h5>
        </div>
        <div className="blog">
          <h5>Blog</h5>
        </div>
        <div className="layanan">
          <h5>Layanan</h5>
        </div>
        <div className="karir">
          <h5>Karir</h5>
        </div>
        <div className="pusat">
          <h5>Pusat Media</h5>
        </div>
      </div>
    );
  }
}
