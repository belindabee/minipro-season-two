import React, { Component } from "react";
import "../asset/asset.css";

export default class Search extends Component {
  render() {
    return (
      <div className="search">
        <input type="text" name="name" placeholder="search movies here..." />
      </div>
    );
  }
}
