import React, { Component } from "react";
import Logo from "../components/Logo";
import BelindaTv from "../components/BelindaTv";
import Search from "../components/Search";
import SignInMenu from "../components/SignInMenu";
import SlidePhotoMenu from "../components/SlidePhotoMenu";
import BrowsCategory from "../components/BrowsCategory";
import List from "../components/List";
import Cards from "../components/Cards";

export default class Home extends Component {
  render() {
    return (
      <div>
        <Logo />
        <BelindaTv />
        <Search />
        <SignInMenu />
        <SlidePhotoMenu />
        <BrowsCategory />
        <List />
        <Cards />
      </div>
    );
  }
}
