import React from "react";
import Logo from "../components/Logo";
import BelindaTv from "../components/BelindaTv";
import Search from "../components/Search";
import Photo from "../components/Photo";
import Review from "../components/Review";
import Title from "../components/Title";
import RatingStar from "../components/RatingStar";
import RatingReview from "../components/RatingReview";
import Sypnosis from "../components/Sypnosis";
import Overview from "../components/Overview";
import BodyExplanation from "../components/BodyExplanation";
import Footer from "../components/Footer";
import MiddleFooter from "../components/MiddleFooter";
import Download from "../components/Download";

export default class Signin extends React.Component {
  render() {
    return (
      <div>
        <Logo />
        <BelindaTv />
        <Search />
        <Photo />
        <Review />
        <Title />
        <RatingStar />
        <RatingReview />
        <Sypnosis />
        <Overview />
        <BodyExplanation />
        <Footer />
        <MiddleFooter />
        <Download />
      </div>
    );
  }
}
