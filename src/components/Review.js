import React from "react";
import image4 from "../asset/images/image4.jpg";

export default class Review extends React.Component {
  render() {
    return (
      <div className="jumbotron">
        <img src={image4} alt="anime" />
      </div>
    );
  }
}
