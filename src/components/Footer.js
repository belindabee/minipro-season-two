import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="logos">
          <div className="logos-1">
            <div className="tvss">
              <h3>Belinda's TV</h3>
            </div>
          </div>
        </div>
        <div className="footer-1">
          <h5>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </h5>
        </div>
      </div>
    );
  }
}
