import React, { Component } from "react";

export default class BodyExplanation extends Component {
  render() {
    return (
      <div className="syp">
        <h1>Sypnosis</h1>
        <div className="syp-1">
          <h5>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum
          </h5>
        </div>
        <div className="info">
          <h1>Movie Info</h1>
          <h5>
            {" "}
            Director: Kei'ichi Sato Writers: Masami Kurumada (manga), Chihiro
            Suzuki (screenplay) | 1 more credit » Stars: Kaito Ishikawa, Kenji
            Akabane, Kenshô Ono | See full cast & crew
          </h5>
        </div>
      </div>
    );
  }
}
