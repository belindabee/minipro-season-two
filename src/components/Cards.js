import React, { Component } from "react";
import tokyo from "../asset/images/tokyo.jpg";
import naruto from "../asset/images/naruto.webp";

import "../asset/asset.css";

export default class Cards extends Component {
  render() {
    return (
      <div className="row">
        <div className="column">
          <div className="card">
            <img src={tokyo} alt="tokyo" />
            <h3>Tokyo Ghoul</h3>
            <p>Anime</p>
          </div>
        </div>

        <div className="row-1">
          <div className="column-1">
            <img src={naruto} alt="naruto" />
            <h3>Naruto Shippuden</h3>
            <p>Anime</p>
          </div>
        </div>
      </div>
    );
  }
}
