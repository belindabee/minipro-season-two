import React from "react";
import noa from "../asset/images/noa.jpg";

export default class Photo extends React.Component {
  render() {
    return (
      <div className="photo">
        <img src={noa} alt="roronoa-zoro" />
      </div>
    );
  }
}
