import React from "react";

export default class List extends React.Component {
  render() {
    return (
      <div>
        <div className="all">
          <h3>All</h3>
        </div>
        <div className="anime">
          <h3>Anime</h3>
        </div>
        <div className="action">
          <h3>Actions</h3>
        </div>
        <div className="adventure">
          <h3>Adventure</h3>
        </div>
        <div className="science">
          <h3>Science Fictions</h3>
        </div>
        <div className="comedy">
          <h3>Comedy</h3>
        </div>
      </div>
    );
  }
}
